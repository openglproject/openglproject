//
// Created by matro on 23.10.2021.
//

#ifndef OPENGLPROJECT_BUTTON_H
#define OPENGLPROJECT_BUTTON_H

#include "../objects/models/GameObject.h"
#include "../objects/default/Image.h"
#include "../demo/Config.h"
#include <functional>

typedef void(CallBack)();

class Button {
private:
//    const int SRC_WIDTH = 1680;
//    const int SRC_HEIGHT = 1050;
    GameObject* buttonImg;
    int x, y, width, height;
    glm::mat4 model;
    std::function<CallBack> callBack;
    bool FindPoint(int x1, int y1, int x2,
                   int y2, int x, int y)
    {
        if (x > x1 and x < x2 and y > y1 and y < y2)
            return true;

        return false;
    }
public:
    Button(const char *texture, int x, int y, int width, int height, std::function<CallBack> callBack) {
        this->callBack = callBack;
        this->x = x;
        this->y = y;
        this->height = height;
        this->width = width;
        this->buttonImg = Image(texture).getRectangle();
        this->model = glm::mat4(1.f);
        float x_ndc = 2.0f * (x + 0.5f) / (float)SRC_WIDTH - 1.0f;
        float y_ndc = 2.0f * (y + 0.5f) / (float)SRC_HEIGHT - 1.0f;
        float width_ndc = std::abs(2.0f * (this->width + 0.5) / (float)SRC_WIDTH - 1.0f);
        float height_ndc = std::abs(2.0f * (this->height + 0.5) / (float)SRC_WIDTH - 1.0f);
        this->model = glm::translate(this->model, glm::vec3(x_ndc, y_ndc, 0.000001f));
//        this->model = glm::scale(this->model, glm::vec3((float )width/(float)SRC_WIDTH, (float )height/(float)SRC_HEIGHT, 1.f));
        this->model = glm::scale(this->model, glm::vec3(width_ndc, height_ndc, 0.f));
    }
    void draw(Shader* shader) {
        shader->setMat4("model", this->model);
        this->buttonImg->Draw(shader);
    }

    void checkClick(double mx, double my) {
        int x_start = this->x - this->width / 2;
        int x_end = x_start + this->width;
        int y_start = SRC_HEIGHT - this->y - this->height / 2;
        int y_end = y_start + this->height;
//        debug(x_start, x_end, y_start, y_end);
        if(FindPoint(x_start, y_start, x_end, y_end, mx, my)){
            this->callBack();
            std::cout<<"click";
        }
    }

//#define _DEBUG
#ifdef _DEBUG
#include "../objects/models/ModelLoader.h"
    static void debug(int x1, int x2, int y1, int y2, Shader* shader) {
        vector<Vertex> vertices{
                Vertex{.Position=glm::vec3(x1, y1, 1.0), .TexCoords=glm::vec2(1.0, 1.0)},
                Vertex{.Position=glm::vec3(x1, y2, 1.0), .TexCoords=glm::vec2(1.0, 0.0)},
                Vertex{.Position=glm::vec3(x2, y1, 1.0), .TexCoords=glm::vec2(0.0, 0.0)},
                Vertex{.Position=glm::vec3(x2, y2, 1.0), .TexCoords=glm::vec2(0.0, 1.0)},
        };
        vector<unsigned int> indices{
                0, 1, 3,
                1, 2, 3,
        };
        string texture = "resources/menu/debug/red.png";
        vector<Texture> textures;
        Texture t;
        string directory = texture.substr(0, texture.find_last_of('/'));
        string name = texture.substr(texture.find_last_of('/')+1);
        int texture_width, texture_height;
        t.id = TextureFromFile(name.c_str(), directory, texture_width, texture_height);
        t.type = "texture_diffuse";
        textures.push_back(t);

        Mesh m(vertices, indices, textures);
        GameObject(vector<Mesh>{m}).Draw(shader);
    }
#endif
};


#endif //OPENGLPROJECT_BUTTON_H
