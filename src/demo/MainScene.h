#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>
#include "../core/IScene.h"
#include "../shaders/Shader.h"

#include "../objects/player/PlayerInput.h"
#include "../utils/SpaceGenerator.h"

#include "../objects/models/ModelLoadedFactory.h"
#include "../objects/default/ParallelogramLoader.h"
#include "../objects/SkyBox.h"
#include "../objects/default/Image.h"

#include "Config.h"
#include "../objects/models/model.h"

class MainScene :
	public IScene
{
private:
	Shader* ourShader;
	Shader* border;
    Shader* skyBoxShader;
    Shader* asteroidShader;

    float f = 0;

    Player* player;
    PlayerInput* playerInput;

    std::vector<PlanetSystem*>* planetD;
    std::map<Models ,GameObject*>* ourModels;
    GameObject* spaceShip;
    Model* rock;

    GameObject* skybox;
    SpaceGenerator *generator;
public:
    void Init() override {
        ourShader = new Shader("shaders/Vertex.vs.shader",
                               "shaders/Pixel.fs.shader");
        // use almost the same vertex shader with one change
        // we multiply normals vertex in glPosition to correct borders work
        border = new Shader("shaders/Border.vs.shader",
                            "shaders/Border.fs.shader");
        skyBoxShader = new Shader("shaders/skybox.vs.shader",
                                  "shaders/skybox.fs.shader");

        asteroidShader = new Shader("shaders/asteroids.vs.glsl",
                                    "shaders/asteroids.fs.glsl");


        vector<string> faces{
             "resources/skybox/blue/right.png",
             "resources/skybox/blue/left.png",
             "resources/skybox/blue/bottom.png",
             "resources/skybox/blue/top.png",
             "resources/skybox/blue/front.png",
             "resources/skybox/blue/back.png",
        };
        SkyBox s(faces);
        skybox = s.create();

        planetD = new std::vector<PlanetSystem*>();

        //std::vector<glm::vec4>* add = generator->PlanetCoordCreate(27);
        //planetD->insert(planetD->end(), add->begin(), add->end());
        //"resources/5/scene.gltf",
        std::vector<string> paths = {"resources/5/scene.gltf", "resources/spaceship/scene.gltf", "resources/stars/sun.obj"};

        ModelLoadedFactory factory(paths);

        ourModels = new map<Models ,GameObject*>();
        std::vector <GameObject*> o1 = factory.loadAll();
        ourModels->insert(pair<Models, GameObject*>(Star, o1[2]));
        ourModels->insert(pair<Models, GameObject*>(SpaceShip, o1[1]));
        ourModels->insert(pair<Models, GameObject*>(Planet, o1[0]));

        rock = new Model("resources/rock/rock.obj");


        std::cout<<ourModels->size()<<std::endl;
        spaceShip = o1[1];

        player = new Player(spaceShip);
        playerInput = new PlayerInput(player);

        generator = new SpaceGenerator(ourModels, rock);
        planetD = generator->generate(player->getPosition());

        this->setInput(playerInput);

    }

	void draw(double deltaTime) override {
        glClearColor(0.1f, 0.4f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

        // don't forget to enable shader before setting uniforms
        Camera* camera = playerInput->getCamera();
        glm::mat4 projection = glm::perspective(glm::radians(30.f + camera->getZoom()), (float)SRC_WIDTH / (float)SRC_HEIGHT, 0.1f, 1000.0f);
        glm::mat4 view = camera->getViewMatrix();//camera->getViewMatrix();
        glm::vec3 viewPos = camera->getPosition();
        glm::vec3 lightPos = glm::vec3(sin(f)*10, 0, cos(f)*10);

        border->use();
        border->setMat4("projection", projection);
        border->setMat4("view", view);

        border->setVec3("viewPos", viewPos);
        border->setVec3("lightPos", lightPos);

        asteroidShader->use();
        asteroidShader->setMat4("projection", projection);
        asteroidShader->setMat4("view", view);

        ourShader->use();
        // view/projection transformations
        ourShader->setMat4("projection", projection);
        ourShader->setMat4("view", view);
        
        ourShader->setVec3("viewPos", viewPos);
        ourShader->setVec3("lightPos", lightPos);

        // render the loaded model
        player->draw(*ourShader);
        playerInput->move(deltaTime);
#define DRAW_PLANET
#ifdef DRAW_PLANET

        for (int i = 0; i < planetD->size(); i++) {
            planetD->at(i)->draw(ourShader, asteroidShader, deltaTime);
            //glm::mat4 model = glm::mat4(1.0f);
            //glm::vec4 c = (glm::vec4)planetD->at(i);
            //model = glm::translate(model, glm::vec3(c.x, c.y, c.z));
            //model = glm::scale(model, glm::vec3(c.w, c.w, c.w));
            //ourShader->use();
            //ourShader->setMat4("model", model);
            //((GameObject*)ourModels->at("planet"))->Draw(ourShader);
            //// use scale too see borders
            //float scale = 1.03f;
            //model = glm::scale(model, glm::vec3(scale, scale, scale));
            //border->use();
            //border->setMat4("model", model);
            //((GameObject*)ourModels->at("planet"))->DrawBorder(border);
        }
#endif

        glDepthFunc(GL_LEQUAL);
        skyBoxShader->use();

        // change depth function so depth test passes when values are equal to depth buffer's content
        view = glm::lookAt(glm::vec3(0, 0, 0), camera->getFront(), camera->getUp());
        skyBoxShader->setMat4("view", view);
        skyBoxShader->setMat4("projection", projection);
        skybox->Draw(skyBoxShader);

        glDepthFunc(GL_LESS); // set depth function back to default*/

    }

	void onResize(int width, int height) override {}
};