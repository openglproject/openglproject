//
// Created by matro on 23.10.2021.
//

#ifndef OPENGLPROJECT_CONFIG_H
#define OPENGLPROJECT_CONFIG_H
enum Models{
    SpaceShip,
    Planet,
    Star,
    Rock
};

const int SRC_WIDTH = 1680;
const int SRC_HEIGHT = 1050;
#endif //OPENGLPROJECT_CONFIG_H
