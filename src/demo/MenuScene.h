#pragma clang diagnostic push
#pragma ide diagnostic ignored "hicpp-signed-bitwise"
#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "../core/IScene.h"
#include "../shaders/Shader.h"

#include "../objects/models/GameObject.h"
#include "../objects/player/Camera.h"
#include "../objects/models/ModelLoader.h"

#include "../demo/MainScene.h"
#include "../objects/text/FontFactory.h"
#include "../objects/default/Image.h"
#include "../ui/Button.h"
#include "Config.h"

class MenuScene:
    public IScene, public IInput
{
private:

    Shader* ourShader{};
    Shader* textShader{};

    Camera* camera{};
    Font* font{};
    Button* newGameButton;
    Button* exitButton;
    GameObject* background;
public:

    void Init() override
    {
        ourShader = new Shader("shaders/Vertex.vs.shader",
                "shaders/Pixel.fs.shader");
        textShader = new Shader("shaders/VertexText.vs",
                "shaders/PixelText.ps");

        //std::vector<string> paths = { "resources/5/scene.gltf" };
        auto* loader = new FontLoader("resources/Arimo-Regular.ttf");
        font = new Font(loader->getCharacters());
        camera = new Camera(glm::vec3(0.0f, 0.0f, 3.0f));
        this->setInput(this);

        std::vector<string> paths = {"resources/spaceship/scene.gltf"};
        ModelLoadedFactory factory(paths);
//        newGameButton = new Button("resources/menu/buttons/newGame.png", 10, -100, 100, 100, [](){});
        newGameButton = new Button("resources/menu/buttons/newGame.png", SRC_WIDTH/2, SRC_HEIGHT/2+130, SRC_WIDTH/5, SRC_WIDTH/20, [this](){
            this->game->setScene(new MainScene());
        });
        exitButton = new Button("resources/menu/buttons/exit.png", SRC_WIDTH/2, SRC_HEIGHT/2-130, SRC_WIDTH/5, SRC_WIDTH/20, [this](){
            exit(0);
        });

        background = Image("resources/menu/background/menuBackground.png").getRectangle();

    }

    void draw(double deltaTime) override {

        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

        // don't forget to enable shader before setting uniforms
        // view/projection transformations
        glm::mat4 projection = glm::perspective(glm::radians(camera->getZoom()), (float)SRC_WIDTH / (float)SRC_HEIGHT, 0.1f, 100.0f);
        glm::mat4 view = camera->getViewMatrix();
        ourShader->use();

        ourShader->setMat4("projection", projection);
        ourShader->setMat4("view", view);

        ourShader->setVec3("viewPos", camera->getPosition());
        //glm::vec3 lightPos = glm::vec3(10, 0, 10);
        //ourShader->setVec3("lightPos", lightPos);

        glm::mat4 model = glm::mat4(1.f);
        model = glm::scale(model,glm::vec3( hypot(3, 4)/2.f,  hypot(3, 4)/2.f, 0.f));
        ourShader->setMat4("model", model);
        background->Draw(ourShader);

        newGameButton->draw(ourShader);
        exitButton->draw(ourShader);

        //font->drawText(*textShader,
        //        "Press space to start",(float)SRC_WIDTH/6 , (float)SRC_HEIGHT/2, 0.25f, glm::vec3(0.5, 0.8f, 0.2f));
    }

    void onResize(int width, int height) override {}

    void processInput(GLFWwindow* window, double deltaTime) override
    {
        if (glfwGetKey(window, /*GLFW_KEY_ESCAPE*/GLFW_KEY_SPACE) == GLFW_PRESS) {
            this->game->setScene(new MainScene());
        }
        if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
            glfwSetWindowShouldClose(window, true);
    }
    void mouse_callback(GLFWwindow* window, double xpos, double ypos) override { }
    void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) override { }

    void mouse_click_callback(GLFWwindow *pWwindow, int button, int action, int mods) override {
        if(button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS){
            double xpos, ypos;
            glfwGetCursorPos(pWwindow, &xpos, &ypos);
            newGameButton->checkClick(xpos, ypos);
            std::cout<<"x: " << xpos << " y: " << ypos << std::endl;
        }
    }
    void new_game_callback() {
        this->game->setScene(new MainScene());
    }
};
