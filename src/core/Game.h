#pragma once
#include <iostream>
#include <glad.h>
#include <GLFW/glfw3.h>
#include "IScene.h"

class Game
{
public:
    Game(IScene* scene, int width, int height);
    void setScene(IScene* pScene);
    void start();
	~Game() = default;
private:
	// settings
	int SRC_WIDTH ;
	int SRC_HEIGHT;
	// timing
	double deltaTime = 0.0;
	double lastFrame = 0.0;

	IScene* scene{};
	IInput* input{};

    GLFWwindow* window{};

	void initialization() {
        glfwInit();
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

        #ifdef __APPLE__
                glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
        #endif

    // glfw window creation
    // --------------------
        window = glfwCreateWindow(SRC_WIDTH, SRC_HEIGHT, "Engine-0.0.1-dev-3", nullptr, nullptr);
        scene->setWindow(window);
        if (window == nullptr)
        {
            std::cout << "Failed to create GLFW window" << std::endl;
            glfwTerminate();
            exit( -1);
        }
        glfwMakeContextCurrent(window);
        glfwSetWindowUserPointer(window, this);
        auto framebuffer_size_func = [](GLFWwindow* wwindow, int width, int height)
        {
            static_cast<Game*>(glfwGetWindowUserPointer(wwindow))->framebuffer_size_callback(wwindow, width, height);
        };
        glfwSetFramebufferSizeCallback(window, framebuffer_size_func);
        auto mousefunc = [](GLFWwindow* wwindow, double xpos, double ypos)
        {
            static_cast<Game*>(glfwGetWindowUserPointer(wwindow))->input->mouse_callback(wwindow, xpos, ypos);
        };
        glfwSetCursorPosCallback(window, mousefunc);
        auto scrollfunc = [](GLFWwindow* wwindow, double xoffset, double yoffset)
        {
            static_cast<Game*>(glfwGetWindowUserPointer(wwindow))->input->scroll_callback(wwindow, xoffset, yoffset);
        };
        glfwSetScrollCallback(window, scrollfunc);
        auto mouseClick = [](GLFWwindow* wwindow, int button, int x, int y)
        {
            static_cast<Game*>(glfwGetWindowUserPointer(wwindow))->input->mouse_click_callback(wwindow, button, x, y);
        };
        glfwSetMouseButtonCallback(window, mouseClick);

        // tell GLFW to capture our mouse
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

        // glad: load all OpenGL function pointers
        // ---------------------------------------
        if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
        {
            std::cout << "Failed to initialize GLAD" << std::endl;
            exit(-1);
        }

        // configure global opengl state
        // -----------------------------
        glEnable(GL_STENCIL_TEST);
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
        glEnable(GL_DEPTH_TEST);
        // build and compile shaders
        // -------------------------
        scene->Init();
        input = scene->getInput();
	}

    /* Callbacks */
    void framebuffer_size_callback(GLFWwindow* window, int width, int height)
    {
        scene->onResize(width, height);
        // make sure the viewport matches the new window dimensions; note that width and
        // height will be significantly larger than specified on retina displays.
        glViewport(0, 0, width, height);
    }
    void processInput(GLFWwindow* pGlfWwindow)
    {
        input->processInput(pGlfWwindow, deltaTime);
    }
};
void Game::start() {
    initialization();
    std::cout << "init complete" << std::endl;
    glEnable( GL_BLEND );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    while (!glfwWindowShouldClose(window))
    {
        // per-frame time logic
        // --------------------
        double currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        // input
        // -----
        processInput(window);

        // render
        // ------

        scene->draw(deltaTime);

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    exit(0);
}

void Game::setScene(IScene* pScene) {
//    std::cout << this << std::endl;
    if (this->scene == nullptr)
        std::cout << "nullptr" << std::endl;
    else
        this->scene = pScene;
    //scene->setGame(this);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    scene->setWindow(window);
    scene->Init();
    input = scene->getInput();
    this->scene->setGame(this);
}

Game::Game(IScene* scene, int width, int height)
{
    this->scene = scene;
    this->SRC_WIDTH = width;
    this->SRC_HEIGHT = height;
    this->scene->setGame(this);
    //scene->setGame(this);
}


