#pragma once
#include <GLFW/glfw3.h>
#include "IInput.h"
#include "Game.h"

class Game;

class IScene
{
private:

	IInput* input{};
protected:
	Game* game{};
	GLFWwindow* window{};

public:
	virtual void draw(double deltaTime) = 0;
	virtual void onResize(int width, int height) = 0;
	virtual void Init() = 0;
	void setWindow(GLFWwindow* pWwindow) { this->window = pWwindow; };
	IInput* getInput() { return this->input; };
	void setInput(IInput* pInput) { this->input = pInput; };
	void setGame(Game* pGame) { this->game = pGame; };
	//Game* getGame() { return this->game; };
};

