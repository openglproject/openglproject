#include "core/Game.h"
#include "demo/MenuScene.h"
#include "demo/Config.h"

using namespace std;

int main()
{
    Game* g = new Game(new MenuScene(), SRC_WIDTH, SRC_HEIGHT);
    g->start();
    return 0;
}