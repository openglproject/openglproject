//
// Created by matro on 06.06.2020.
//

#ifndef OPENGLPROJECT_PLANETSYSTEM_H
#define OPENGLPROJECT_PLANETSYSTEM_H

#include <vector>
#include "../objects/models/GameObject.h"
#include "../demo/Config.h"
#include <map>
#include "../demo/AsteroidField.h"
#include "../objects/models/model.h"

class PlanetSystem {
public:
    explicit PlanetSystem(glm::vec3& center, map<Models, GameObject *> *pMap, Model *rock) {
        this->pMap = pMap;
        glm::mat4 model = glm::mat4(1.f);
        model = glm::scale(model, glm::vec3(20.f, 20.f, 20.f));
        this->star = model;
        for(int i = 0; i < 5; i++){
            glm::mat4 model = glm::mat4(1.f);
            model = glm::scale(model, glm::vec3((float )i));
            model = glm::translate(model, glm::vec3(i*40+35, 1.f, 1.f));
            m.emplace_back(model, pMap->at(Planet));
        }
        std::cout<<"size: "<<m.size()<<endl;
        asteroidField = new AsteroidField(glm::vec3(0, 0, 0), rock, 150.f, 20.f, 20000);
    }
    PlanetSystem();

    void draw(Shader *s, Shader *asteroidShader, float delta) {
        s->use();
        s->setMat4("model", this->star);
        pMap->at(Star)->Draw(s);
        char a = 0;
        for (auto &it: m) {
            s->setMat4("model", it.first);
            it.first = glm::rotate(glm::mat4(1.f), glm::radians(10.f*(float)a++*delta), glm::vec3(0.f, 1.f, 0.f))*it.first;
            it.second->Draw(s);
        }

        if(asteroidField!= nullptr) {
            asteroidField->draw(asteroidShader);
        }

    }

private:
    glm::mat4 star;
    std::vector<std::pair<glm::mat4, GameObject*>> m{};
    map<Models, GameObject *> *pMap;
    AsteroidField* asteroidField;

    uint32_t nProcGen = 0;

    float rndFloat(float min, float max) {
        return ((float) rnd() / (float) (0x7FFFFFFF)) * (max - min) + min;
    }

    int rndInt(int min, int max) {
        return (rnd() % (max - min)) + min;
    }

    uint32_t rnd() {
        nProcGen += 0xe120fc15;
        uint64_t tmp;
        tmp = (uint64_t) nProcGen * 0x4a39b70d;
        uint32_t m1 = (tmp >> 32) ^tmp;
        tmp = (uint64_t) m1 * 0x12fad5c9;
        uint32_t m2 = (tmp >> 32) ^tmp;
        return m2;
    }
};

PlanetSystem::PlanetSystem() {

}



#endif //OPENGLPROJECT_PLANETSYSTEM_H
