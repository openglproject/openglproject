#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "../objects/GameObject.h"
#include<map>
    class PlanetSystem {
    public:
        void DrawPlanets(Shader s)
        {
            for(auto & it: m)
            {
                s.setVec3("model",it.first);
                it.second->Draw();
            }

        }
    PlanetSystem(std::vector<GameObject*> *vec)
    {
        m =  map<glm::vec4, GameObject*>();
        PlanetCoordCreate(*vec);

    }

    GameObject DefinePlanet(std::vector<GameObject> vec )
    {
        int ModelId=rndInt(0,vec.size()-1);
        return vec[ModelId];

    }
private:
    map<glm::vec4, GameObject*> m;


    void PlanetCoordCreate(std::vector<GameObject*> vec)
    {
        glm::vec4 StarCoord;
        StarCoord.x = 0;
        StarCoord.y = 0;
        StarCoord.z = 0;
        StarCoord.w = rndFloat(3.0f, 10.0f);
        int PlanetN = rndInt(2, 6);
        glm::vec4 FirstPLanet;
        float radius = rndFloat(1.0f, 2.0f);
        FirstPLanet.x =radius +StarCoord.w * 0.5;
        FirstPLanet.y = radius  + StarCoord.w * 0.5;
        FirstPLanet.z = radius + StarCoord.w * 0.5;
        FirstPLanet.w = rndFloat(1.0f, 4.0f);
        int ModelId=rndInt(0,vec.size());
        GameObject* PlanetModel=vec[ModelId];
        m[FirstPLanet]=PlanetModel;
        for (int i = 1; i < PlanetN; i++)
        {
            float radius = rndFloat(1.0f, 2.0f);
            int ModelId=rndInt(0,vec.size());
            GameObject* PlanetModel=vec[ModelId];
            glm::vec4 Planet;
            Planet.x = radius + FirstPLanet.w * 0.5;
            Planet.y = radius + FirstPLanet.w * 0.5;
            Planet.z = radius + FirstPLanet.w * 0.5;
            Planet.w = rndFloat(1.0f, 3.0f);
            m[Planet]=PlanetModel;
            Planet = FirstPLanet;
        }
    }
    uint32_t nProcGen = 0;
    float rndFloat(float min, float max)
    {
        return ((float)rnd() / (float)(0x7FFFFFFF)) * (max - min) + min;
    }

    int rndInt(int min, int max)
    {
        return (rnd() % (max - min)) + min;
    }

    uint32_t rnd()
    {
        nProcGen += 0xe120fc15;
        uint64_t tmp;
        tmp = (uint64_t)nProcGen * 0x4a39b70d;
        uint32_t m1 = (tmp >> 32) ^ tmp;
        tmp = (uint64_t)m1 * 0x12fad5c9;
        uint32_t m2 = (tmp >> 32) ^ tmp;
        return m2;
    }

};