#pragma once
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <cmath>
#include "PlanetSystem.h"
#include "../objects/models/model.h"

class SpaceGenerator {
public:

    SpaceGenerator(map<Models, GameObject *> *pMap, Model *rock) {
        this->pMap = pMap;
        this->rock = rock;
    }
    std::vector<PlanetSystem*>* generate(glm::vec3 pos){
        glm::vec3 p = glm::vec3(0);
        return new vector<PlanetSystem*>{new PlanetSystem(p, pMap, rock)};
    }

    /*std::vector<glm::vec4>* PlanetCoordCreate(glm::vec3 pos) {
        int sizeSectorX = 170;
        int sizeSectorY = 170;
        int sizeSectorZ = 170;
        std::vector<glm::vec4> *planetD = new std::vector<glm::vec4>();
        float size = 5;
        int k = 2;
        int tx = ((int)pos.x+sizeSectorX/4)/(sizeSectorX/2)-k;
        int ty = ((int)pos.y+sizeSectorY/4)/(sizeSectorY/2)-k;
        int tz = ((int)pos.z+sizeSectorZ/4)/(sizeSectorZ/2)-k;
        for(int x = tx; x < size + tx; x++) {
            for (int y = ty; y < size + ty; y++) {
                for (int z = tz; z < size + tz; z++) {
                    glm::vec4 temp;
                    temp.x = x * sizeSectorX/2;//x*nSectorX - size*nSectorX/4;
                    temp.y = y * sizeSectorY/2;//y*nSectorY - size*nSectorY/4;
                    temp.z = z * sizeSectorZ/2;//z*nSectorZ - size*nSectorZ/4;
                    temp.w = 0.1;//rndFloat(0.1f, 4.0f, x+ y + z + seed);
                    planetD->push_back(temp);
                }
            }
        }
        return planetD;
    }*/

private:
    map<Models, GameObject *> *pMap;
    Model *rock;
    uint32_t seed = 0xe120fc15+1;
    uint32_t nProcGen = 0xe120fc15;

    float rndFloat(float min, float max, int seed)
    {
        return ((float)rnd(seed) / (float)(0x7FFFFFFF)) * (max - min) + min;
    }

    int rndInt(int min, int max, int seed)
    {
        return (rnd(seed) % (max - min)) + min;
    }

    uint32_t rnd(int seed)
    {
        nProcGen += seed;
        uint64_t tmp;
        tmp = (uint64_t)nProcGen * 0x4a39b70d;
        uint32_t m1 = (tmp >> 32) ^ tmp;
        tmp = (uint64_t)m1 * 0x12fad5c9;
        uint32_t m2 = (tmp >> 32) ^ tmp;
        return m2;
    }
    bool setPlanetOrNot(int seed)
    {
        int n = rndInt(0, 10, seed);
        std::cout<<"random:"<<n<<"\n";
        return n == 1;
    }
};

