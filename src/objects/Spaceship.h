//
// Created by vspirate on 14.06.2020.
//
#include "models/GameObject.h"

namespace spaceship_attributes {
    enum Movement {
        FORWARD,
        BACKWARD,
        LEFT,
        RIGHT
    };
    const float YAW = -90.0f;
    const float PITCH = 0.0f;
    const float ROLL = 0.0f;
    const float SPEED = 8.0f;
    const float ROTATION = 130.f;
    const float SLOW = 0.3f;
}

class Spaceship {
private:
    GameObject* model;
    glm::vec3* position;
    glm::vec3* angles;

    float acceleration;
    float rotationSpeed;

public:
    Spaceship(GameObject* m, bool isPlayer = false)
    {
        model = m;

        rotationSpeed = spaceship_attributes::ROTATION;
        acceleration = spaceship_attributes::SPEED;

        angles = new glm::vec3(spaceship_attributes::YAW, spaceship_attributes::PITCH, spaceship_attributes::ROLL);
        position = new glm::vec3(0.0f);
    };
    ~Spaceship()
    {
        delete model;
        delete position;
        delete angles;
    }

    void setPosition(glm::vec3 pos) { *position = pos; }
    void setAngles(glm::vec3 ang) { *angles = ang; }

    float getAcceleration() const { return acceleration; }
    float getRotationSpeed() const { return rotationSpeed; }

    // without distance, we can`t pin spaceship to camera
    void draw(Shader& shader, glm::vec3 distance = glm::vec3(0.f))
    {
        shader.use();
        glm::mat4 modelMatrix = glm::mat4(1.f);
        modelMatrix = glm::translate(modelMatrix, *position);
        modelMatrix = glm::rotate(modelMatrix, -glm::radians(angles->x), glm::vec3(0, 1, 0));
        modelMatrix = glm::rotate(modelMatrix, glm::radians(angles->y), glm::vec3(0, 0, 1));
        modelMatrix = glm::rotate(modelMatrix, -glm::radians(angles->z), glm::vec3(1, 0, 0));
        modelMatrix = glm::translate(modelMatrix, distance);
        // by default spaceship looks to left.
        // to avoid that we rotate them 90 degrees
        modelMatrix = glm::rotate(modelMatrix, glm::radians(90.f), glm::vec3(0, 1, 0));
        shader.setMat4("model", modelMatrix);
        model->Draw(&shader);
    }
};