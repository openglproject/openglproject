//
// Created by vspirate on 14.06.2020.
//
#include "../Spaceship.h"
class Player
{
private:
    Spaceship* spaceship;
    glm::vec3* position;
public:
    Player(GameObject* model)
    {
        spaceship = new Spaceship(model, true);
        position = new glm::vec3(1);
    }
    ~Player()
    {
        delete position;
        delete spaceship;
    }

    glm::vec3 getPosition() const { return *position; }
    void setAngels(glm::vec3 ang) { spaceship->setAngles(ang); }
    void setPosition(glm::vec3 pos) {
        *position = pos;
        spaceship->setPosition(pos);
    }

    float getSpeedVelocity() const { return spaceship->getAcceleration(); }
    float getRotationSpeed() const { return spaceship->getRotationSpeed(); }

    // for spaceship
    void move(glm::vec3 pos) { spaceship->setPosition(pos); }
    void draw(Shader& shader) {
        spaceship->draw(shader, glm::vec3(2.44f, -0.50f, 0));
    }

};