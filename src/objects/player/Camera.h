#pragma once
#include <glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
// Defines several possible options for camera movement. Used as abstraction to stay away from window-system specific input methods
namespace cameras_attribute
{
    // Default camera values
    const float YAW = -90.0f;
    const float PITCH = 0.0f;
    const float SENSITIVITY = 1.0f;
    const float ZOOM = 45.0f;
    const float PI = glm::radians(180.f);
}


// An abstract camera class that processes input and calculates the corresponding Euler Angles, Vectors and Matrices for use in OpenGL
class Camera
{
public:
    // Constructor with vectors
    Camera(glm::vec3 Pos = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), float Yaw = cameras_attribute::YAW, float Pitch = cameras_attribute::PITCH)
    {
        position = Pos;
        front = glm::vec3(0.0f, 0.0f, -1.0f);
        worldUp = up;

        mouseSensitivity = cameras_attribute::SENSITIVITY;
        zoom = cameras_attribute::ZOOM;

        yaw = Yaw;
        pitch = Pitch;
        roll = 0.f;
        updateCameraVectors();
    }
    // Constructor with scalar values
    Camera(float posX, float posY, float posZ, float upX, float upY, float upZ, float Yaw, float Pitch)
    {
        position = glm::vec3(posX, posY, posZ);
        front = glm::vec3(0.0f, 0.0f, -1.0f);
        worldUp = glm::vec3(upX, upY, upZ);

        mouseSensitivity = cameras_attribute::SENSITIVITY;
        zoom = cameras_attribute::ZOOM;

        yaw = Yaw;
        pitch = Pitch;
        roll = 0.f;
        updateCameraVectors();
    }

    // Returns the view matrix calculated using Euler Angles and the LookAt Matrix
    glm::mat4 getViewMatrix() const
    {
        return glm::lookAt(position, front+ position, up);
    }

    // for spaceship
    glm::vec3 getPosition() const { return position; }
    const glm::vec3 getFront() const { return front; }
    const glm::vec3 getUp() const { return up; }
    float getZoom() const { return zoom; }

    void setPosition(glm::vec3 pos) { position = pos; }

    glm::vec3 getAngels() { return glm::vec3(yaw, pitch, roll); }

    void setYaw(float rotation)
    {
        yaw = rotation;
        updateCameraVectors();
    }
    void setPitch(float rotation)
    {
        pitch = rotation;
        updateCameraVectors();
    }
    void setRoll(float rotation)
    {
        roll = rotation;
        updateCameraVectors();
    }

    // Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
    virtual void processMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch = true)
    {
        xoffset *= mouseSensitivity;
        yoffset *= mouseSensitivity;

        yaw += xoffset;
        pitch += yoffset;

        // Make sure that when pitch is out of bounds, screen doesn't get flipped
        if (constrainPitch)
        {
            if (pitch > 89.0f)
                pitch = 89.0f;
            if (pitch < -89.0f)
                pitch = -89.0f;
        }

        // Update center, Right and up Vectors using the updated Euler angles
        updateCameraVectors();
    }

    // Processes input received from a mouse scroll-wheel event. Only requires input on the vertical wheel-axis
    void processMouseScroll(float yoffset)
    {
        if (zoom >= 1.0f && zoom <= 45.0f)
            zoom -= yoffset;
        if (zoom <= 1.0f)
            zoom = 1.0f;
        if (zoom >= 45.0f)
            zoom = 45.0f;
    }
protected:
    // Camera options
    float mouseSensitivity;
    float zoom;

    // Camera Attributes
    glm::vec3 position;
    glm::vec3 front;
    glm::vec3 up;
    glm::vec3 right;
    glm::vec3 worldUp;

    // Euler Angles
    float yaw;
    float pitch;
    float roll;

    // Calculates the center vector from the Camera's (updated) Euler Angles
    void updateCameraVectors() {
        // Calculate the new front vector
        front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
        front.y = sin(glm::radians(pitch));
        front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
        front = glm::normalize(front);
        // Also re-calculate the Right and up vector
        right = glm::normalize(glm::cross(front, worldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
        up = glm::normalize(glm::cross(right, front));

    }
};