//
// Created by vspirate on 17.06.2020.
//

#include "../../core/IInput.h"
#include "Player.h"

class PlayerInput : public IInput {
private:
    Player* player;
    Camera* camera;

    // MOVEMENT
    float movementSpeed;
    float speedVelocity;
    float rotationSpeed;

    // MOUSE
    bool firstMouse = true;
    float lastX;
    float lastY;

    void processMovement(spaceship_attributes::Movement direction, double& deltaTime) {
        float roll = camera->getAngels().z;
        switch (direction)
        {
            case spaceship_attributes::FORWARD: {
                movementSpeed += speedVelocity*deltaTime;
                break;
            }
            case spaceship_attributes::BACKWARD: {
                movementSpeed -= speedVelocity * deltaTime;
                break;
            }
            case spaceship_attributes::LEFT: {
                roll -= (deltaTime * rotationSpeed);
                break;
            }
            case spaceship_attributes::RIGHT: {
                roll += (deltaTime * rotationSpeed);
                break;
            }
        }
        if (roll >= 360.f)
            roll = 0.f;

        camera->setRoll(roll);
        player->setAngels(camera->getAngels());

        if (movementSpeed >= spaceship_attributes::SPEED) movementSpeed = spaceship_attributes::SPEED;
        else if (movementSpeed < -spaceship_attributes::SLOW/2) movementSpeed = -spaceship_attributes::SLOW/2;
    }

    void downSpeed(float deltaTime)
    {
        float velocity = speedVelocity*deltaTime;
        // when we had gone forward
        if(movementSpeed > velocity)
            movementSpeed -= velocity;
            // when we had gone backward
        else if(movementSpeed < -velocity)
            movementSpeed += velocity;
        else
            movementSpeed = 0;
    }

public:
    PlayerInput(Player* play) {
        player = play;
        movementSpeed = 0;
        speedVelocity = player->getSpeedVelocity();
        rotationSpeed = player->getRotationSpeed();
        camera = new Camera(glm::vec3(0.0f, 0.0f, 3.0f));
    }
    ~PlayerInput() {
        delete camera;
        delete player;
    }

    Camera* getCamera() const { return camera; }

    void processInput(GLFWwindow* window, double deltaTime) override
    {
        if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
            glfwSetWindowShouldClose(window, true);

        if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
            processMovement(spaceship_attributes::FORWARD, deltaTime);
        else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
            processMovement(spaceship_attributes::BACKWARD, deltaTime);
        else
            downSpeed(deltaTime);

        if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
            processMovement(spaceship_attributes::LEFT, deltaTime);
        }
        if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
            processMovement(spaceship_attributes::RIGHT, deltaTime);
        }
    }
    void mouse_callback(GLFWwindow* window, double xpos, double ypos) override
    {
        if (firstMouse)
        {
            lastX = xpos;
            lastY = ypos;
            firstMouse = false;
        }

        float xoffset = xpos - lastX;
        float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

        lastX = xpos;
        lastY = ypos;

        camera->processMouseMovement(xoffset, yoffset);
        player->setAngels(camera->getAngels());
    }
    void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) override
    {
        camera->processMouseScroll(yoffset);
    }

    /* TODO: made the same function for rotation with moving mouse */

    // made braking smoother+spaceship_attributes::SLOW
    void move(float deltaTime)
    {
        glm::vec3 pos = camera->getPosition() + camera->getFront()*((movementSpeed+spaceship_attributes::SLOW)*deltaTime);
        camera->setPosition(pos);
        player->setPosition(pos);
    }
};