#pragma once

#include "ModelLoader.h"

class ModelLoadedFactory
{
public:
	ModelLoadedFactory(std::vector<std::string> paths);
	ModelLoadedFactory(std::string path, glm::mat4 trans);
	ModelLoadedFactory(std::string path);
	~ModelLoadedFactory();

	GameObject* load() {
        modelloader = new ModelLoader();
		modelloader->loadModel(paths[paths.size()-1]);
		paths.pop_back();
		GameObject* obj = new GameObject(modelloader->getMeshes());
		obj->setTransform(trans);
		return obj;
	}
	GameObject* load(std::string path, glm::mat4 trans) {
	    modelloader = new ModelLoader();
		modelloader->loadModel(path);
		paths.pop_back();
		GameObject* obj = new GameObject(modelloader->getMeshes(), modelloader->getTextures());
		obj->setTransform(trans);
		return obj;
	}

	std::vector<GameObject*> loadAll() {
		std::vector<GameObject*> loaded;
		for (int i = 0; i < paths.size(); i++){
			modelloader->loadModel(paths[i]);
			loaded.push_back(new GameObject(modelloader->getMeshes()));
		}
		return loaded;
	}

private:
	std::vector<std::string> paths;
	glm::mat4 trans;
	ModelLoader *modelloader;
};

inline ModelLoadedFactory::ModelLoadedFactory(std::vector<std::string> paths)
{
	this->paths = paths;
	this->modelloader = new ModelLoader();
}

inline ModelLoadedFactory::ModelLoadedFactory(std::string path, glm::mat4 trans) {
	this->paths = std::vector<std::string>{ path };
	this->trans = trans;
}

inline ModelLoadedFactory::ModelLoadedFactory(std::string path)
{
	this->paths = std::vector<std::string>{ path };
	this->trans = glm::mat4(0);
}

ModelLoadedFactory::~ModelLoadedFactory()
{
}

