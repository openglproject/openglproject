#pragma once
#include <glm/gtc/matrix_transform.hpp>
#include "Shader.h"
#include <vector>

using std::vector;
using std::string;

struct Vertex {
    // position
    glm::vec3 Position;
    // normal
    glm::vec3 Normal;
    // texCoords
    glm::vec2 TexCoords;
    // tangent
    glm::vec3 Tangent;
    // bitangent
    glm::vec3 Bitangent;
};
struct Material {
    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;
    float shininess;
};

struct Texture {
    unsigned int id;
    string type;
    string path;
};

class Mesh {
public:
    /*  Functions  */
    // constructor
    Mesh(vector<Vertex> vertices, vector<unsigned int> indices, vector<Texture> textures) : vertices(vertices), indices(indices), textures(textures) { setupMesh(); }
    Mesh(vector<Vertex> vertices, vector<unsigned int> indices, vector<Texture> textures, Material material) : vertices(vertices), indices(indices), textures(textures), material(material) { setupMesh(); }
    Mesh(vector<Vertex> vertices, vector<Texture> textures) : vertices(vertices), textures(textures) { setupMesh(); }
    // render the mesh
    void Draw(Shader* shader);

/*  Render data  */
    unsigned int VAO;
    vector<unsigned int> indices;
private:
    /*  Mesh Data  */
    vector<Vertex> vertices;
    vector<Texture> textures;
    Material material = {glm::vec3(1), glm::vec3(1), glm::vec3 (1), 64};

    /*  Render data  */
    unsigned int VBO, EBO;
    glm::mat4 transform;

    // initializes all the buffer objects/arrays
    void setupMesh();
};