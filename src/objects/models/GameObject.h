#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include "Mesh.h"

class GameObject
{
public:
    GameObject() {};
    GameObject(vector<Mesh> meshes) : meshes(meshes), transfer(glm::mat4(1.0f)) {}
    GameObject(vector<Mesh> meshes, vector<Texture> textures_loaded) : meshes(meshes), textures_loaded(textures_loaded), transfer(glm::mat4(1.0f)) {}

    // draws the model, and thus all its meshes
    void Draw(Shader *shader)
    {
        for (unsigned int i = 0; i < meshes.size(); i++) {
            meshes[i].Draw(shader);
        }
    }

    void setTransform(glm::mat4 trans) { this->transfer = trans; }
    glm::mat4 getTransform() { return transfer; }

    // use after method Draw
    void DrawBorder(Shader* shader)
    {
        glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
        glStencilMask(0xFF);
        this->Draw(shader);
        glStencilFunc(GL_ALWAYS, 1, 0xFF);

    }

// stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.
    vector<Mesh> meshes;
    vector<Texture> textures_loaded;
protected:
    /*  Model Data */
    string directory;
    glm::mat4 transfer = glm::mat4(1.0f);
};

#endif