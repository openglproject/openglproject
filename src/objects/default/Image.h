#pragma once
#include "../models/GameObject.h"
#include "../models/ModelLoader.h"
class Image
{
public:
	Image(string texture) {
		Texture t;
		string directory = texture.substr(0, texture.find_last_of('/'));
		string name = texture.substr(texture.find_last_of('/')+1);
        int texture_width, texture_height;
		t.id = TextureFromFile(name.c_str(), directory, texture_width, texture_height, false, true);
		t.type = "texture_diffuse";
		this->textures.push_back(t);
		this->rectangle = makeRectangle(texture_width, texture_height);
	}
	GameObject* getRectangle() {
		return rectangle;
	}

private:
	GameObject* rectangle;
	vector<Texture> textures;
    GameObject* makeRectangle(int &texture_width, int & texture_height) {
        //float coefx = 1.f/((float)texture_width/(float)texture_height);
        //float coefy = ((float)texture_height/(float)texture_width)/4;
        glm::vec2 size = glm::normalize(glm::vec2(texture_width, texture_height));
		vector<Vertex> vertices{
			Vertex{.Position=glm::vec3( size.x,  size.y, 0.0), .TexCoords=glm::vec2(1.0, 1.0)},
			Vertex{.Position=glm::vec3( size.x, -size.y, 0.0), .TexCoords=glm::vec2(1.0, 0.0)},
			Vertex{.Position=glm::vec3(-size.x, -size.y, 0.0), .TexCoords=glm::vec2(0.0, 0.0)},
			Vertex{.Position=glm::vec3(-size.x,  size.y, 0.0), .TexCoords=glm::vec2(0.0, 1.0)},
		};
		vector<unsigned int> indices{
			0, 1, 3,
			1, 2, 3,
		};

		Mesh m(vertices, indices, this->textures);
		return new GameObject(vector<Mesh>{m});
	}
    unsigned int TextureFromFile(const char* path, const string& directory, int &width, int &height, bool gamma = false, bool inverse = true)
    {
        string filename = string(path);
        filename = directory + '/' + filename;

        std::cout << filename << endl;

        unsigned int textureID;
        glGenTextures(1, &textureID);

        int nrComponents;
        stbi_set_flip_vertically_on_load(inverse);
        unsigned char* data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
        if (data)
        {
            GLenum format;
            if (nrComponents == 1)
                format = GL_RED;
            else if (nrComponents == 3)
                format = GL_RGB;
            else if (nrComponents == 4)
                format = GL_RGBA;

            glBindTexture(GL_TEXTURE_2D, textureID);
            glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            stbi_image_free(data);
        }
        else
        {
            std::cout << "Texture failed to load at path: " << path << std::endl;
            stbi_image_free(data);
        }

        return textureID;
    }
};


