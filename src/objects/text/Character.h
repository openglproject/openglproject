#pragma once
#include <glm/vec2.hpp>
// to avoid errors with Character in different files we wrote this in another file
struct Character {
    unsigned int TextureID;  // ID handle of the glyph texture
    glm::ivec2   Size;       // Size of glyph
    glm::ivec2   Bearing;    // Offset from baseline to left/top of glyph
    long Advance;           // Offset to advance to next glyph
};
