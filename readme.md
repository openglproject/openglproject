# OpenGL Project

This is Richelieu lyceums Project that was created to improve our skills in programming and math.


## Dependencies

In project we use libraries:
* glm
* freetype
* GLFW
* STL


## Usage

Install submodules
```
git submodule init
git submodule update
```

Write in CMake options
```
-DASSIMP_BUILD_ALL_IMPORTERS_BY_DEFAULT=NO
-DASSIMP_BUILD_OBJ_IMPORTER=YES
-DASSIMP_BUILD_GLTF_IMPORTER=YES
-DASSIMP_NO_EXPORT=YES
-DASSIMP_BUILD_TESTS=OFF
-DBUILD_SHARED_LIBS=OFF
```

Run
```
cmake CMakeLists.txt
```


## Documentation

### Models

#### Usage

To use all features of ```GameObject``` in file include it and ```ModelLoader``` or ```ModelLoadedFactory```
```
#include "../objects/models/GameObject.h"
#include "../objects/models/ModelLoadedFactory.h"
```

#### Loaders

You can load formats:
* obj
* glTF

To load model use ```ModelLoader``` and create ```GameObject```
```
ModelLoader* modelLoader = new ModelLoader();
modelLoader->loadModel("resources/spaceship/scene.gltf");
GameObject* obj = new GameObject(modelLoader->getMeshes());
```

To load models use ``` ModelLoadedFactory ```
```
std::vector<string> paths = {"resources/planet/untitled.obj", "resources/spaceship/scene.gltf"};
ModelLoadedFactory factory(paths);
std::vector <GameObject*> objects = factory.loadAll();
GameObject* planet = object[0];
GameObject* spaceship = object[1];
```

#### Draw

To draw model create and use ``` Shader ```
```
Shader* shader = new Shader("shaders/Vertex.vs.shader", "shaders/Pixel.fs.shader");
spaceship->Draw(shader);
```

### Font, FontLoader

#### Usage

To use Fonts in Project include it in Scenes files
```
#include "../objects/text/FontFactory.h"
```

#### Load Fonts

To load one Font use ```FontLoader``` and create new ```Font```
```
FontLoader* loader = new FontLoader("resources/Arimo-Regular.ttf");
Font* font = new Font(loader->getCharacters());
```

To load a lot of Fonts use ```FontFactory```
```
FontFactory* factory = new FontFactory({"resources/Arimo-Regular.ttf", "resources/Arimo-Bold.ttf"});
std::vector<Font*> fonts = factory->loadAll();
```

#### Draw text
```
font->drawText(*textShader, "Hello World!",(float)SRC_WIDTH/6 , (float)SRC_HEIGHT/2, 0.5f, glm::vec3(0.5, 0.8f, 0.2f));
```
